# Battleship AI

An AI that is supposed to compete with other AIs via intermediate server according to [this](https://avoronkov.gitlab.io/oop/18201.cpp/task4/) I/O protocol

### Build

    $ mkdir build
    $ cd build
    $ cmake .. && make

