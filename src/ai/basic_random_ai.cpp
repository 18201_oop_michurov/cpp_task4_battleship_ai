#include <algorithm>
#include <random>
#include "basic_random_ai.hpp"


BasicRandomAI::BasicRandomAI(
        BasicField & enemy_field)
        :
        enemy_field(enemy_field)
{}


void BasicRandomAI::processDamageDealt(
        Player::DamageDealt damage_dealt)
{
    auto last_attacked_cell = BasicRandomAI::lastAttackedCell();

    switch (damage_dealt)
    {
        case Player::DamageDealt::Hit:
        case Player::DamageDealt::Kill:
            this->enemy_field[last_attacked_cell] = BasicField::Cell::Hit;
            break;
        case Player::DamageDealt::Miss:
            this->enemy_field[last_attacked_cell] = BasicField::Cell::Miss;
            break;
    }

    this->updateUncheckedCells();
}


void BasicRandomAI::resetUncheckedCells(
        std::vector<BasicField::Position> const & new_cells_vector)
{
    this->unchecked_cells = new_cells_vector;
}


BasicField::Position BasicRandomAI::fire() const
{
    if (this->unchecked_cells.empty())
    {
        throw game_over("No more positions to fire at! Game has ended!");
    }

    return this->unchecked_cells.back();
}


BasicField::Position BasicRandomAI::lastAttackedCell() const
{
    return this->unchecked_cells.back();
}


void BasicRandomAI::updateUncheckedCells()
{
    this->unchecked_cells.pop_back();
}


void BasicRandomAI::resetUncheckedCells()
{
    this->unchecked_cells.clear();

    for (unsigned char x = 'A'; x < 'A' + BasicField::FieldSize; ++x)
    {
        for (unsigned char y = 0; y < BasicField::FieldSize; ++y)
        {
            if (this->enemy_field[{x, y}] == BasicField::Cell::FogOfWar)
            {
                this->unchecked_cells.emplace_back(x, y);
            }
        }
    }

    std::shuffle(
            this->unchecked_cells.begin(),
            this->unchecked_cells.end(),
            std::mt19937(std::random_device()()));
}
