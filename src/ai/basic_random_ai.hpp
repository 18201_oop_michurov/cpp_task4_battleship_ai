#ifndef BATTLESHIP_BASIC_RANDOM_AI_HPP
#define BATTLESHIP_BASIC_RANDOM_AI_HPP


#include "field/basic_field.hpp"
#include "src/player/player.hpp"


class BasicRandomAI
{
protected:
    explicit BasicRandomAI(BasicField & enemy_field);

    void resetUncheckedCells(std::vector<BasicField::Position> const & new_cells_vector);

    virtual void resetUncheckedCells();

    virtual void updateUncheckedCells();

    BasicField::Position lastAttackedCell() const;

public:
    BasicRandomAI() = delete;
    virtual ~BasicRandomAI() = default;

    virtual void processDamageDealt(
            Player::DamageDealt damage_dealt);

    BasicField::Position fire() const;

protected:
    BasicField & enemy_field;

private:
    std::vector<BasicField::Position> unchecked_cells;
};


#endif //BATTLESHIP_BASIC_RANDOM_AI_HPP
