#ifndef BATTLESHIP_OBSERVANT_AI_HPP
#define BATTLESHIP_OBSERVANT_AI_HPP


#include "src/ai/basic_random_ai.hpp"
#include "src/player/player.hpp"


class ObservantAI : public BasicRandomAI
{
protected:
    void updateUncheckedCells() override;

    unsigned char getLastDamagedShipSize() const;

    explicit ObservantAI(BasicField & enemy_filed, nullptr_t);

public:
    explicit ObservantAI(BasicField & enemy_field);

    void processDamageDealt(
            Player::DamageDealt damage_dealt) override;

private:
    std::vector<BasicField::Position> last_damaged_ship;
};


#endif //BATTLESHIP_OBSERVANT_AI_HPP
