#include "strategist_ai.hpp"


#include <algorithm>
#include <random>


StrategistAI::StrategistAI(
        BasicField & enemy_field)
        :
        ObservantAI(enemy_field, nullptr),
        enemy_ship_count{{4, 1}, {3, 2}, {2, 3}, {1, 4}},
        misses(0)
{
    // std::cout << "StrategistAI(BasicField &)" << std::endl;
    this->StrategistAI::resetUncheckedCells();
}


void StrategistAI::resetUncheckedCells()
{
    // std::cout << "StrategistAI::resetUncheckedCells()" << std::endl;
    // std::cout << enemy_ship_count[4] << " " << enemy_ship_count[3] << " " << enemy_ship_count[2]
    //            << " " << enemy_ship_count[1] << std::endl;

    std::vector<BasicField::Position> unchecked_cells;

    cell_count_t decks = 4;

    if (this->enemy_ship_count[4] == 0)
    {
        decks = 3;
        if (this->enemy_ship_count[3] == 0)
        {
            decks = 2;
            if (this->enemy_ship_count[2] == 0)
            {
                decks = 1;
                if (this->enemy_ship_count[1] == 0)
                {
                    return;
                    // throw game_over("Enemy has no ships left! Game has ended!");
                }
            }
        }
    }

    std::vector<BasicField::Position> pattern;

    pattern.reserve(decks);

    for (int i = 0; i < decks; ++i)
    {
        pattern.emplace_back(static_cast<unsigned char>('A' + i), static_cast<unsigned char>(decks - i - 1));
    }

    for (unsigned char x = 'A'; x < BasicField::FieldSize + 'A'; x += decks)
    {
        for (unsigned char y = 0; y < BasicField::FieldSize; y += decks)
        {
            for (auto const & position : pattern)
            {
                if (BasicField::Position::isValid(position.x + x - 'A', position.y + y)
                    and this->enemy_field[
                                {
                                        static_cast<unsigned char>(position.x + x - 'A'),
                                        static_cast<unsigned char>(position.y + y)
                                }] == BasicField::FogOfWar)
                {
                    unchecked_cells.emplace_back(position.x + x - 'A', position.y + y);
                }
            }
        }
    }

    if (decks > 1)
    {
        std::shuffle(
                unchecked_cells.begin(),
                unchecked_cells.end(),
                std::mt19937(std::random_device()()));

        if (this->misses < 5)
        {
            std::stable_sort(
                    unchecked_cells.begin(),
                    unchecked_cells.end(),
                    [](BasicField::Position const & p1, BasicField::Position const & p2) -> bool
                    {
                        auto dx = std::abs(p1.x - static_cast<double>('A' + 'A' + BasicField::FieldSize - 1) / 2);
                        auto dy = std::abs(p1.y - static_cast<double>(BasicField::FieldSize - 1) / 2);

                        auto d1 = dx * dx + dy * dy;

                        dx = std::abs(p2.x - static_cast<double>('A' + 'A' + BasicField::FieldSize - 1) / 2);
                        dy = std::abs(p2.y - static_cast<double>(BasicField::FieldSize - 1) / 2);

                        auto d2 = dx * dx + dy * dy;

                        return d1 < d2;
                    }
            );
        }
    }
    else
    {
        std::map<BasicField::Position, cell_count_t> surrounding_cells_count;

        for (auto const & position : unchecked_cells)
        {
            auto pv = this->enemy_field.getSurroundings(1, true, position);
            surrounding_cells_count[position] = std::count_if(
                    pv.begin(),
                    pv.end(),
                    [this](BasicField::Position const & p) -> bool
                    {
                        return this->enemy_field[p] == BasicField::Cell::FogOfWar;
                    });
        }

        std::sort(
                unchecked_cells.begin(),
                unchecked_cells.end(),
                [&surrounding_cells_count](BasicField::Position const & p1, BasicField::Position const & p2)
                {
                    return surrounding_cells_count[p1] < surrounding_cells_count[p2];
                });
    }

    BasicRandomAI::resetUncheckedCells(unchecked_cells);
}


void StrategistAI::processDamageDealt(
        Player::DamageDealt damage_dealt)
{
    // std::cout << "StrategistAI::processDamageDealt()";

    if (damage_dealt == Player::DamageDealt::Kill)
    {
        this->misses = 0;
        cell_count_t ship_size = this->ObservantAI::getLastDamagedShipSize() + 1;
        this->enemy_ship_count[ship_size] -= 1;
    }
    else if (damage_dealt == Player::DamageDealt::Hit)
    {
        this->misses = 0;
    }

    ObservantAI::processDamageDealt(damage_dealt);

    // std::cout << " -> ObservantAI::processDamageDealt()" << std::endl;

    if (damage_dealt == Player::DamageDealt::Miss)
    {
        this->misses += 1;

        if ((this->enemy_ship_count[4] == 0
            and this->enemy_ship_count[3] == 0
            and this->enemy_ship_count[2] == 0)
            or this->misses > 4) // assume that there are one cell ships left
        {
            this->ObservantAI::updateUncheckedCells();
            this->StrategistAI::resetUncheckedCells();
        }
    }
}
