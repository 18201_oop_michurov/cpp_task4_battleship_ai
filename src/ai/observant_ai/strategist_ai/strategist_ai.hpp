#ifndef BATTLESHIP_STRATEGIST_AI_HPP
#define BATTLESHIP_STRATEGIST_AI_HPP


#include "ai/observant_ai/observant_ai.hpp"


class StrategistAI : public ObservantAI
{
private:

protected:
    void resetUncheckedCells() override;

public:
    explicit StrategistAI(BasicField & enemy_field);

    void processDamageDealt(
            Player::DamageDealt damage_dealt) override;

private:
    std::unordered_map<unsigned char, size_t> enemy_ship_count;
    size_t misses;
};


#endif //BATTLESHIP_STRATEGIST_AI_HPP
