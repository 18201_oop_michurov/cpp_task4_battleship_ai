#include <algorithm>
#include <random>


#include "observant_ai.hpp"


ObservantAI::ObservantAI(
        BasicField & enemy_field,
        nullptr_t)
        :
        BasicRandomAI(enemy_field)
{
    // std::cout << "ObservantAI(BasicField &, nullptr_t)" << std::endl;
}


ObservantAI::ObservantAI(
        BasicField & enemy_field)
        :
        BasicRandomAI(enemy_field)
{
    // std::cout << "ObservantAI(BasicField &)" << std::endl;
    this->BasicRandomAI::resetUncheckedCells();
}


void ObservantAI::processDamageDealt(
        Player::DamageDealt damage_dealt)
{
    auto last_attacked_cell = this->BasicRandomAI::lastAttackedCell();

    if (damage_dealt == Player::DamageDealt::Miss)
    {
        this->enemy_field[last_attacked_cell] = BasicField::Cell::Miss;
    }
    else
    {
        this->enemy_field[last_attacked_cell] = BasicField::Cell::Hit;

        this->last_damaged_ship.push_back(last_attacked_cell);
        std::sort(this->last_damaged_ship.begin(), this->last_damaged_ship.end());

// #define NEXT_POSITIONS_OUTPUT
#ifdef NEXT_POSITIONS_OUTPUT
        std::cout << "Known enemy ship cells:" << std::endl;
    for (auto const & p : this->last_damaged_ship)
    {
        std::cout << p << ", ";
    }
    std::cout << std::endl;
#endif

        if (damage_dealt == Player::DamageDealt::Kill)
        {
            auto surroundings = this->enemy_field.getSurroundings(
                    this->last_damaged_ship.size(),
                    this->last_damaged_ship.front().y == this->last_damaged_ship.back().y,
                    this->last_damaged_ship.front());

            for (auto const & position : surroundings)
            {
                if (this->enemy_field[position] == BasicField::Cell::FogOfWar)
                {
                    this->enemy_field[position] = BasicField::Cell::Water;
                }
            }

            this->last_damaged_ship.clear();
        }
    }

    this->ObservantAI::updateUncheckedCells();

// #define NEXT_POSITIONS_OUTPUT
#ifdef NEXT_POSITIONS_OUTPUT
    std::cout << "Hmmm imma bout to check these next:" << std::endl;
    for (auto const & p : this->unchecked_positions)
    {
        std::cout << p << ", ";
    }
    std::cout << std::endl << std::endl;
#endif
}


void ObservantAI::updateUncheckedCells()
{
    auto last_attacked_cell = this->BasicRandomAI::lastAttackedCell();

    if (this->enemy_field[last_attacked_cell] == BasicField::Cell::Miss)
    {
        this->BasicRandomAI::updateUncheckedCells();
    }
    if (this->enemy_field[last_attacked_cell] == BasicField::Cell::Hit)
    {
        if (this->last_damaged_ship.empty())
        {
            // std::cout << "ObservantAI::updateUncheckedCells() -> this->resetUncheckedCells()" << std::endl;
            this->resetUncheckedCells();
        }
        else
        {
            std::vector<BasicField::Position> unchecked_cells;

            if (this->last_damaged_ship.size() == 1)
            {
                int x = last_attacked_cell.x;
                int y = last_attacked_cell.y;

                if (BasicField::Position::isValid(x - 1, y) and this->enemy_field[
                        {static_cast<unsigned char>(x - 1), static_cast<unsigned char>(y)}] == BasicField::Cell::FogOfWar)
                {
                    unchecked_cells.emplace_back(
                            static_cast<unsigned char>(x - 1),
                            static_cast<unsigned char>(y));
                }
                if (BasicField::Position::isValid(x + 1, y) and this->enemy_field[
                        {static_cast<unsigned char>(x + 1), static_cast<unsigned char>(y)}] == BasicField::Cell::FogOfWar)
                {
                    unchecked_cells.emplace_back(
                            static_cast<unsigned char>(x + 1),
                            static_cast<unsigned char>(y));
                }
                if (BasicField::Position::isValid(x, y - 1) and this->enemy_field[
                        {static_cast<unsigned char>(x), static_cast<unsigned char>(y - 1)}] == BasicField::Cell::FogOfWar)
                {
                    unchecked_cells.emplace_back(
                            static_cast<unsigned char>(x),
                            static_cast<unsigned char>(y - 1));
                }
                if (BasicField::Position::isValid(x, y + 1) and this->enemy_field[
                        {static_cast<unsigned char>(x), static_cast<unsigned char>(y + 1)}] == BasicField::Cell::FogOfWar)
                {
                    unchecked_cells.emplace_back(
                            static_cast<unsigned char>(x),
                            static_cast<unsigned char>(y + 1));
                }
            }
            else
            {
                int dx = this->last_damaged_ship.front().y == this->last_damaged_ship.back().y;
                int dy = this->last_damaged_ship.front().x == this->last_damaged_ship.back().x;

                if (BasicField::Position::isValid(
                        this->last_damaged_ship.front().x - dx, this->last_damaged_ship.front().y - dy)
                    and this->enemy_field[
                                {
                                        static_cast<unsigned char>(this->last_damaged_ship.front().x - dx),
                                        static_cast<unsigned char>(this->last_damaged_ship.front().y - dy)
                                }] == BasicField::Cell::FogOfWar)
                {
                    unchecked_cells.emplace_back(
                            static_cast<unsigned char>(this->last_damaged_ship.front().x - dx),
                            static_cast<unsigned char>(this->last_damaged_ship.front().y - dy));
                }
                if (BasicField::Position::isValid(
                        this->last_damaged_ship.back().x + dx, this->last_damaged_ship.back().y + dy)
                    and this->enemy_field[
                                {
                                        static_cast<unsigned char>(this->last_damaged_ship.back().x + dx),
                                        static_cast<unsigned char>(this->last_damaged_ship.back().y + dy)
                                }] == BasicField::Cell::FogOfWar)
                {
                    unchecked_cells.emplace_back(
                            static_cast<unsigned char>(this->last_damaged_ship.back().x + dx),
                            static_cast<unsigned char>(this->last_damaged_ship.back().y + dy));
                }
            }

            std::shuffle(
                    unchecked_cells.begin(),
                    unchecked_cells.end(),
                    std::mt19937(std::random_device()()));

            this->BasicRandomAI::resetUncheckedCells(unchecked_cells);
        }
    }
}


unsigned char ObservantAI::getLastDamagedShipSize() const
{
    return this->last_damaged_ship.size();
}
