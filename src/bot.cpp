#include "driver.hpp"


int main()
{
    try
    {
        runBot<StrategistAI>();
    }
    catch (std::exception & e)
    {
        std::cout << "Error: " << e.what() << std::endl;

        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}