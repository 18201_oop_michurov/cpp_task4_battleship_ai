#include <iostream>
#include <regex>
#include <fstream>


#include "field/basic_field.hpp"
#include "player/dummy/dummy.hpp"
#include "ai/observant_ai/observant_ai.hpp"
#include "ai/observant_ai/strategist_ai/strategist_ai.hpp"


template <typename AIType1, typename AIType2>
inline unsigned int runTestBattle(bool output = true)
{
    Dummy unit01((Tag<AIType1>()));
    Dummy unit03((Tag<AIType2>()));

    size_t unit01_kill_count = 0;
    size_t unit03_kill_count = 0;

    Player * player1 = &unit01;
    Player * player2 = &unit03;

    size_t swaps = 0;

    if (output)
    {
        std::cout << "Dummy 1's field:\n" << unit01.getField().toString(false, true) << std::endl << std::endl;

        std::cout << "Dummy 2's field:\n" << unit03.getField().toString(false, true) << std::endl << std::endl;
    }

    Player::DamageDealt damage_dealt;

    for (size_t niter = 0; niter < 500; ++niter)
    {
        size_t & kill_count = swaps % 2 == 0 ? unit01_kill_count : unit03_kill_count;
        size_t & other_kill_count = swaps % 2 == 0 ? unit03_kill_count : unit01_kill_count;
        
        try
        {
            do
            {
                BasicField::Position pos = player1->fire();

                if (output)
                {
                    std::cout << (swaps % 2 == 0 ? "Dummy 1" : "Dummy 2") << " fires at " << pos << ": ";
                }

                damage_dealt = player2->processIncomingDamage(pos);

                if (output)
                {
                    std::cout << damage_dealt << "!" << std::endl;
                }

                if (damage_dealt == Player::DamageDealt::Kill)
                {
                    kill_count++;

                    if (kill_count == 10)
                    {
                        if (output)
                        {
                            std::cout << (swaps % 2 == 0 ? "Dummy 1" : "Dummy 2")
                                        << " has won the game! 10 enemy ships destroyed!" << std::endl;
                            std::cout << (swaps % 2 ? "Dummy 1" : "Dummy 2") << "'s kill count: "
                                        << other_kill_count << std::endl;
                        }
                        return 1u + swaps % 2u;
                    }
                }

                player1->processDamageDealt(damage_dealt);
            }
            while (damage_dealt == Player::DamageDealt::Hit or damage_dealt == Player::DamageDealt::Kill);
        }
        catch (game_over & e)
        {
            throw std::runtime_error("Game has ended without anyone scoring 10 kills. An error must have occurred.");
        }

        std::swap(player1, player2);
        swaps++;
    }

    return 1;
}


template <typename AIType>
inline void runBot()
{
    Dummy unit00((Tag<AIType>()));

    std::string input;

    std::regex regex("^Enemy shooted into ([A-J]) ([0-9])$");
    std::cmatch match;

    std::ofstream wins("winlog.txt", std::ofstream::app);

    for (size_t i = 0; i < 2500; ++i)
    {
        input.clear();
        std::getline(std::cin, input);

        if (input == "Arrange!")
        {
            std::cout << unit00.getField().toString(true, false) << std::endl;
        }
        else if (input == "Shoot!")
        {
            std::cout << unit00.fire() << std::endl;
        }
        else if (input == "Miss")
        {
            unit00.processDamageDealt(Player::DamageDealt::Miss);
        }
        else if (input == "Hit")
        {
            unit00.processDamageDealt(Player::DamageDealt::Hit);

            std::cout << unit00.fire() << std::endl;
        }
        else if (input == "Kill")
        {
            unit00.processDamageDealt(Player::DamageDealt::Kill);

            std::cout << unit00.fire() << std::endl;
        }
        else if (input == "Win!" or input == "Lose" or std::cin.fail())
        {
            // std::ofstream wins("winlog.txt");
            if (std::cin.fail())
            {
                wins << "cin fail" << std::endl;
                throw std::runtime_error("Input stream closed");
            }

            if (input == "Win!")
            {
                wins << "win" << std::endl;
            }

            if (input == "Lose")
            {
                wins << "lose" << std::endl;
            }

            return;
        }
        else if (input.empty())
        {
            continue;
        }
        else
        {
            std::regex_match(input.c_str(), match, regex);

            if (not match.empty())
            {
                unsigned char x = input[19];
                unsigned char y = input[21] - '0';

                std::cout << unit00.processIncomingDamage({x, y}) << std::endl;
            }
            else
            {
                std::cout << "\nbad server data format: no match for \"^Enemy shooted into ([A-J]) ([0-9])$\" in "
                             "\"" << input.c_str() << "\"" << std::endl;

                wins << "bad server data format : " + input << std::endl;
                throw std::logic_error("Bad server data format");
            }
        }
    }

    wins << "too many turns" << std::endl;

    throw std::runtime_error("Too many turns");
}
