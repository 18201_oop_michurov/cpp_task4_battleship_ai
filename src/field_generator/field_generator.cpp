#include <algorithm>


#include "field/player_field/player_field.hpp"
#include "utility/utility_functions.hpp"


#include "field_generator.hpp"


struct AvailablePosition : public BasicField::Position
{
    AvailablePosition(
            unsigned char x,
            unsigned char y,
            bool is_horizontal,
            cell_count_t surrounding_cells_count)
            :
            BasicField::Position(x, y),
            is_horizontal(is_horizontal),
            surrounding_cells_count(surrounding_cells_count)
    {}

    bool operator!=(AvailablePosition const & other) const
    {
        return this->x != other.x or this->y != other.y or this->is_horizontal != other.is_horizontal;
    }

    bool operator==(AvailablePosition const & other) const
    {
        return !(*this != other);
    }

    bool is_horizontal;
    cell_count_t surrounding_cells_count;
};


static inline std::vector<AvailablePosition> getAvailablePositions(
        PlayerField const & field,
        cell_count_t ship_size,
        BasicField::Position const & top_left_corner,
        BasicField::Position const & bottom_right_corner)
{
    std::vector<AvailablePosition> available_positions;

    for (unsigned char x = top_left_corner.x; x <= bottom_right_corner.x; ++x)
    {
        for (unsigned char y = top_left_corner.y; y <= bottom_right_corner.y; ++y)
        {
            BasicField::Position current_position = {x, y};

            if (field[current_position] == BasicField::Cell::Water)
            {
                auto result = field.canPlace(
                        ship_size,
                        true,
                        current_position,
                        top_left_corner,
                        bottom_right_corner);

                if (result.first)
                {
                    available_positions.emplace_back(x, y, true, result.second);
                }

                result = field.canPlace(
                        ship_size,
                        false,
                        current_position,
                        top_left_corner,
                        bottom_right_corner);

                if (result.first and ship_size > 1)
                {
                    available_positions.emplace_back(x, y, false, result.second);
                }
            }
        }
    }

    return available_positions;
}


static inline bool tryPlaceShips(
        PlayerField & field,
        cell_count_t ship_size)
{
    if (ship_size < 1 or ship_size > 4)
    {
        throw std::out_of_range("Ship size must be between at least 1 and 4 at most.");
    }

    std::vector<AvailablePosition> available_positions;

    size_t position_index;

    for (size_t i = 0; i < 5 - ship_size; ++i)
    {
        if (ship_size == 1)
        {
            available_positions = getAvailablePositions(field, ship_size, {'A', 0}, {'J', 9});
        }
        else
        {
            std::vector<AvailablePosition> auxiliary_vector;

            available_positions = getAvailablePositions(
                    field,
                    ship_size,
                    {'A', 0},
                    {'J', static_cast<unsigned char>(4 - ship_size)}
            ); // top horizontal rectangle

            auxiliary_vector = getAvailablePositions(
                    field,
                    ship_size,
                    {'A', 0},
                    {static_cast<unsigned char>('A' + 4 - ship_size), 9}
            ); // left vertical rectangle
            available_positions.insert(available_positions.end(), auxiliary_vector.begin(), auxiliary_vector.end());

            auxiliary_vector = getAvailablePositions(
                    field,
                    ship_size,
                    {static_cast<unsigned char>('J' - 4 + ship_size), 0},
                    {'J', 9}
            ); // right vertical rectangle
            available_positions.insert(available_positions.end(), auxiliary_vector.begin(), auxiliary_vector.end());

            auxiliary_vector = getAvailablePositions(
                    field,
                    ship_size,
                    {'A', static_cast<unsigned char>(9 - 4 + ship_size)},
                    {'J', 9}
            ); // bottom horizontal rectangle
            available_positions.insert(available_positions.end(), auxiliary_vector.begin(), auxiliary_vector.end());

            if (available_positions.empty())
            {
                return false;
            }

            std::sort(
                    available_positions.begin(),
                    available_positions.end(),
                    [](AvailablePosition const & p1, AvailablePosition const p2) -> bool
                    {
                        return p1.surrounding_cells_count < p2.surrounding_cells_count;
                    }
            );

            available_positions.erase(
                    std::unique(
                            available_positions.begin(),
                            available_positions.end()
                    ),
                    available_positions.end()
            );

            auto min_cells_count = available_positions.front().surrounding_cells_count;

            available_positions.erase(
                    std::find_if(
                            available_positions.begin(),
                            available_positions.end(),
                            [min_cells_count](AvailablePosition const & p) -> bool
                            {
                                return p.surrounding_cells_count != min_cells_count;
                            }
                    ),
                    available_positions.end()
            );
        }

        position_index = utility::randomIndex(0, available_positions.size() - 1);

        field.placeShip(
                ship_size,
                available_positions[position_index].is_horizontal,
                available_positions[position_index]);
    }

    return true;
}


void placeShips(
        PlayerField & field)
{
    field = PlayerField(BasicField::Cell::Water);

    tryPlaceShips(field, 4);

    auto auxiliary_field = field;

    while (not tryPlaceShips(field, 3) or not tryPlaceShips(field, 2) or not tryPlaceShips(field, 1))
    {
        field = auxiliary_field;
    }

    for (unsigned char x = 'A'; x < 'A' + BasicField::FieldSize; ++x)
    {
        for (unsigned char y = 0; y < BasicField::FieldSize; ++y)
        {
            if (field[{x, y}] == BasicField::NearShip)
            {
                field[{x, y}] = BasicField::Water;
            }
        }
    }
}


std::vector<std::string> field_generator::generateField()
{
    PlayerField new_field(BasicField::Cell::Water);

    placeShips(new_field);

    return new_field.getLines();
}
