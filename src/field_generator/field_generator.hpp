#ifndef BATTLESHIP_FIELD_GENERATOR_HPP
#define BATTLESHIP_FIELD_GENERATOR_HPP


#include <vector>
#include <string>


namespace field_generator
{
    std::vector<std::string> generateField();
}


#endif //BATTLESHIP_FIELD_GENERATOR_HPP
