#include <random>


#include "utility_functions.hpp"


size_t utility::randomIndex(
        size_t min_allowed_index,
        size_t max_allowed_index)
{
    static std::random_device rd;
    static std::mt19937 eng(rd());
    std::uniform_int_distribution<> distribution(min_allowed_index, max_allowed_index);

    return distribution(eng);
}
