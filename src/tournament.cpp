#include "driver.hpp"


int main()
{
    std::cout << "*** EPIC TOURNAMENT ***\n" << std::endl;
    std::unordered_map<int, size_t> wins;

    for (size_t i = 0; i < 100; ++i)
    {
        wins[runTestBattle<ObservantAI, ObservantAI>(false)]++;
    }

    std::cout << "Dummy 1 (ObservantAI) won " << wins[1] << " times" << std::endl;
    std::cout << "Dummy 2 (ObservantAI) won " << wins[2] << " times\n" << std::endl;

    wins.clear();

    for (size_t i = 0; i < 1000; ++i)
    {
        wins[runTestBattle<ObservantAI, StrategistAI>(false)]++;
    }

    std::cout << "Dummy 1 (ObservantAI) won " << wins[1] << " times" << std::endl;
    std::cout << "Dummy 2 (StrategistAI) won " << wins[2] << " times\n" << std::endl;

    wins.clear();

    for (size_t i = 0; i < 1000; ++i)
    {
        wins[runTestBattle<StrategistAI, StrategistAI>(false)]++;
    }

    std::cout << "Dummy 1 (StrategistAI) won " << wins[1] << " times" << std::endl;
    std::cout << "Dummy 2 (StrategistAI) won " << wins[2] << " times\n" << std::endl;

    return 0;
}

