#ifndef BATTLESHIP_PLAYER_HPP
#define BATTLESHIP_PLAYER_HPP


#include <cstdlib>
#include <vector>
#include <string>
#include <unordered_map>


#include "field/basic_field.hpp"


class game_over : public std::logic_error
{
public:
    explicit game_over(std::string const & err_message);
};


class Player
{
public:
    enum class DamageDealt
    {
        Hit,
        Miss,
        Kill
    };

    Player() = default;
    virtual ~Player() = default;

    virtual DamageDealt processIncomingDamage(BasicField::Position position) = 0;

    virtual void processDamageDealt(DamageDealt damage_dealt) = 0;

    virtual BasicField::Position fire() = 0;

    virtual BasicField const & getField() const = 0;
    virtual BasicField const & getEnemyField() const = 0;

    Player(Player const & other) = delete;
    Player(Player && other) = delete;

    Player & operator=(Player const & other) = delete;
    Player & operator=(Player && other) = delete;
};


std::ostream & operator<<(
        std::ostream & os,
        Player::DamageDealt damage);


#endif //BATTLESHIP_PLAYER_HPP
