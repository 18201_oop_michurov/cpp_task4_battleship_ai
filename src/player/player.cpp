#include <stdexcept>


#include "player.hpp"


std::ostream & operator<<(
        std::ostream & os,
        Player::DamageDealt damage)
{
    std::string s;

    switch (damage)
    {
        case Player::DamageDealt::Hit:
            s = "Hit";
            break;
        case Player::DamageDealt::Miss:
            s = "Miss";
            break;
        case Player::DamageDealt::Kill:
            s = "Kill";
            break;
    }

    os << s;

    return os;
}

game_over::game_over(
        std::string const &err_message)
        :
        logic_error(err_message)
{}
