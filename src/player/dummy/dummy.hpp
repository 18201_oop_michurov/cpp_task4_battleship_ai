#ifndef BATTLESHIP_DUMMY_HPP
#define BATTLESHIP_DUMMY_HPP


#include <memory>


#include "player/player.hpp"
#include "ai/basic_random_ai.hpp"
#include "field/player_field/player_field.hpp"
#include "field_generator/field_generator.hpp"


template <typename T>
class Tag
{};


class Dummy : public Player
{
public:
    template <class AIType>
    explicit Dummy(Tag<AIType>)
            :
            field(field_generator::generateField()),
            enemy_field(BasicField::Cell::FogOfWar),
            ships_left(1 + 2 + 3 + 4),
            ai(std::make_unique<AIType>(this->enemy_field))
    {}

    DamageDealt processIncomingDamage(BasicField::Position position) override;

    void processDamageDealt(
            DamageDealt damage_dealt) override;

    BasicField const & getField() const override;
    BasicField const & getEnemyField() const override;

    BasicField::Position fire() override;

private:
    PlayerField field;
    BasicField enemy_field;

    size_t ships_left;

    std::unique_ptr<BasicRandomAI> ai;
};


#endif //BATTLESHIP_DUMMY_HPP
