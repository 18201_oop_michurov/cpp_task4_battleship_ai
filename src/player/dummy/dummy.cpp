#include <algorithm>


#include "dummy.hpp"


BasicField::Position Dummy::fire()
{
    return this->ai->fire();
}


Player::DamageDealt Dummy::processIncomingDamage(
        BasicField::Position position)
{
    if (this->ships_left == 0)
    {
        throw game_over("Game has ended!");
    }

    if (this->field.getShipsInfo().empty())
    {
        throw game_over("Game has ended!");
    }

    if (this->field[position] != BasicField::Cell::Ship)
    {
        this->field[position] = BasicField::Cell::Miss;
        return Player::DamageDealt::Miss;
    }

    auto ship_info = this->field.getShipsInfo().find(position);

    if (ship_info == this->field.getShipsInfo().end() or ship_info->second.get()->empty())
    {
        throw std::logic_error("BasicField data must be corrupted or something");
    }

    this->field[position] = BasicField::Cell::Hit;

    auto ship_cells = ship_info->second.get();

    ship_cells->erase(
            std::find(
                    ship_cells->begin(),
                    ship_cells->end(),
                    position));

    if (ship_cells->empty())
    {
        this->ships_left -= 1;

        return Player::DamageDealt::Kill;
    }

    return Player::DamageDealt::Hit;
}


void Dummy::processDamageDealt(
        Player::DamageDealt damage_dealt)
{
    this->ai->processDamageDealt(damage_dealt);
}


BasicField const & Dummy::getField() const
{
    return this->field;
}


BasicField const & Dummy::getEnemyField() const
{
    return this->enemy_field;
}